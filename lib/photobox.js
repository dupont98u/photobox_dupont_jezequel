let photobox = {
	modules : {}
}

photobox.modules.photoloader = (function () {

	let se;


	

	return{
		init : function (x) {
			se = x;
		},

		nav : {

			n : " ",
			p : " ",

			first : function(){
				photobox.modules.gallery.loadGallery("/www/canals5/photobox/photos/?offset=0&size=10");
			},


			charg : function (x) {
				let pr = $.ajax(se + x , {
					type: 'GET',
					dateType: 'json',
					xhrFields: {
						withCredentials : true,
					}
				});

				pr.done( function( data, status, jqXHR ){ 
					n = data.links.next.href;
					p = data.links.prev.href;
					alert("DONE  : Ajax charged");
				});

				pr.fail(function(d,s,x){
					alert('ERROR : ajax  -> ' + s );
				});
				return pr;
			},


			nextPage : function (){
				console.log("go to : " + n);
				let pr = photobox.modules.gallery.loadGallery(n);
				return pr;
			},


			prevPage : function (){
				console.log("go to : " + p);
				let pr = photobox.modules.gallery.loadGallery(p);
				return pr;
			},
		},


	}



})();

photobox.modules.gallery = (function() {

	let loaderB = document.getElementById("load_gallery");
	let list ;


	return {
		init : function() {
			loaderB.addEventListener("click",(event) => {photobox.modules.gallery.loadGallery("/www/canals5/photobox/photos/?offset=0&size=10")});
			console.log("init");
		},

		loadGallery : function(s) {
			let photoloader = photobox.modules.photoloader;
			photoloader.init('https://webetu.iutnc.univ-lorraine.fr');
			let nav = photoloader.nav;
			let pr = nav.charg(s);
			pr.done(function(data){
				//data.photos[0].links.self.href (Lien image)
				console.log(data.photos[0].photo);
				let vign="";
				list = data.photos;
				let t = 0;
				data.photos.forEach(element => {
					let urlSmall= "https://webetu.iutnc.univ-lorraine.fr"+element.photo.thumbnail.href;
					let urlLarge = "https://webetu.iutnc.univ-lorraine.fr"+element.photo.original.href;
					let title = element.photo.titre;
					vign+='<div class=\"vignette\" onclick=\"photobox.modules.lightbox.openLightBox('+t+')\">';
					vign+='<img data-img='+urlLarge+' src='+urlSmall+'>';
					vign+='<div>'+title+'</div></div>';
					t++;
				});
				let vigns = $(vign);
				console.log(vigns);
				document.getElementById("photobox-gallery").innerHTML=vign;
			})
		},

		getList : function(){
			return list;
		}
		
	}
})();

photobox.modules.lightbox = (function(){

	let index ;

	return{
		closeLightBox : function() {
  			document.getElementById('lightbox').style.display = "none";
		},
		openLightBox : function(x) {
			let temp = photobox.modules.gallery.getList();
			index = x;
			console.log(x);
  			document.getElementById('lightbox').style.display = "block";
  			let cont = '<img src='+"https://webetu.iutnc.univ-lorraine.fr"+temp[index].photo.original.href+'> '
  			cont += ' <div id="lightbox-desc">'+'Titre: '+temp[index].photo.titre+'   ('+temp[index].photo.format+')		'+'Size: '+temp[index].photo.width+'x'+temp[index].photo.height+'</div>';
  			console.log(cont);
			document.getElementById("lightbox_container").innerHTML=cont;
		},
		next : function() {
			lim = photobox.modules.gallery.getList().length;
			if(index>=lim-1)index = -1;
			photobox.modules.photoloader.nav.nextPage();
			console.log(lim);
			index++;
			photobox.modules.lightbox.openLightBox(index);
		},
		prev : function() {
			lim = photobox.modules.gallery.getList().length;
			if(index<=0)index = lim;
			photobox.modules.photoloader.nav.prevPage();
			console.log(lim);
			index--;
			photobox.modules.lightbox.openLightBox(index);
		},
	}
})();


window.addEventListener("load", (event)=>{photobox.modules.gallery.init()});